import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userName = '';
  allowClear = false;

  constructor() { }

  clearUser() {
    this.userName = '';
    this.allowClear = false;
  }

  isUserEmpty() {
    if ( this.userName.trim().length > 0) {
      this.allowClear = true;
    } else {
      this.allowClear = false;
    }
  }

  ngOnInit() {
  }

}
